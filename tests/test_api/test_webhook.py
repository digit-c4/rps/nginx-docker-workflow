from unittest.mock import patch


@patch("ec_rps_workflow.api.router.business.create_mapping_dependencies")
@patch("ec_rps_workflow.api.router.business.destroy_mapping_dependencies")
def test_webhook_created(
    destroy_mapping_dependencies_mock,
    create_mapping_dependencies_mock,
    http_client,
):
    response = http_client.post(
        "/webhook/netbox",
        json={
            "event": "created",
            "timestamp": "2021-01-01T00:00:00Z",
            "model": "mapping",
            "username": "snet",
            "request_id": "123",
            "data": {
                "id": 1,
                "url": "http://netbox.local/path/to/self",
                "created": "2021-01-01T00:00:00Z",
                "last_updated": "2021-01-01T00:00:00Z",
                "source": "https://test.ec.local",
                "target": "https://example.com",
            },
            "snapshots": {
                "prechange": None,
                "postchange": None,
            },
        },
    )

    assert response.status_code == 200
    create_mapping_dependencies_mock.assert_called_once()
    destroy_mapping_dependencies_mock.assert_not_called()


@patch("ec_rps_workflow.api.router.business.create_mapping_dependencies")
@patch("ec_rps_workflow.api.router.business.destroy_mapping_dependencies")
def test_webhook_created_failure(
    destroy_mapping_dependencies_mock,
    create_mapping_dependencies_mock,
    http_client,
):
    create_mapping_dependencies_mock.side_effect = RuntimeError("test")

    response = http_client.post(
        "/webhook/netbox",
        json={
            "event": "created",
            "timestamp": "2021-01-01T00:00:00Z",
            "model": "mapping",
            "username": "snet",
            "request_id": "123",
            "data": {
                "id": 1,
                "url": "http://netbox.local/path/to/self",
                "created": "2021-01-01T00:00:00Z",
                "last_updated": "2021-01-01T00:00:00Z",
                "source": "https://test.ec.local",
                "target": "https://example.com",
            },
            "snapshots": {
                "prechange": None,
                "postchange": None,
            },
        },
    )

    assert response.status_code == 500
    create_mapping_dependencies_mock.assert_called_once()
    destroy_mapping_dependencies_mock.assert_not_called()


@patch("ec_rps_workflow.api.router.business.create_mapping_dependencies")
@patch("ec_rps_workflow.api.router.business.destroy_mapping_dependencies")
def test_webhook_deleted(
    destroy_mapping_dependencies_mock,
    create_mapping_dependencies_mock,
    http_client,
):
    response = http_client.post(
        "/webhook/netbox",
        json={
            "event": "deleted",
            "timestamp": "2021-01-01T00:00:00Z",
            "model": "mapping",
            "username": "snet",
            "request_id": "123",
            "data": {
                "id": 1,
                "url": "http://netbox.local/path/to/self",
                "created": "2021-01-01T00:00:00Z",
                "last_updated": "2021-01-01T00:00:00Z",
                "source": "https://test.ec.local",
                "target": "https://example.com",
            },
            "snapshots": {
                "prechange": None,
                "postchange": None,
            },
        },
    )

    assert response.status_code == 200
    create_mapping_dependencies_mock.assert_not_called()
    destroy_mapping_dependencies_mock.assert_called_once()


@patch("ec_rps_workflow.api.router.business.create_mapping_dependencies")
@patch("ec_rps_workflow.api.router.business.destroy_mapping_dependencies")
def test_webhook_deleted_failure(
    destroy_mapping_dependencies_mock,
    create_mapping_dependencies_mock,
    http_client,
):
    destroy_mapping_dependencies_mock.side_effect = RuntimeError("test")

    response = http_client.post(
        "/webhook/netbox",
        json={
            "event": "deleted",
            "timestamp": "2021-01-01T00:00:00Z",
            "model": "mapping",
            "username": "snet",
            "request_id": "123",
            "data": {
                "id": 1,
                "url": "http://netbox.local/path/to/self",
                "created": "2021-01-01T00:00:00Z",
                "last_updated": "2021-01-01T00:00:00Z",
                "source": "https://test.ec.local",
                "target": "https://example.com",
            },
            "snapshots": {
                "prechange": None,
                "postchange": None,
            },
        },
    )

    assert response.status_code == 500
    create_mapping_dependencies_mock.assert_not_called()
    destroy_mapping_dependencies_mock.assert_called_once()


@patch("ec_rps_workflow.api.router.business.create_mapping_dependencies")
@patch("ec_rps_workflow.api.router.business.destroy_mapping_dependencies")
def test_webhook_wrongzone(
    destroy_mapping_dependencies_mock,
    create_mapping_dependencies_mock,
    http_client,
):
    response = http_client.post(
        "/webhook/netbox",
        json={
            "event": "created",
            "timestamp": "2021-01-01T00:00:00Z",
            "model": "mapping",
            "username": "snet",
            "request_id": "123",
            "data": {
                "id": 1,
                "url": "http://netbox.local/path/to/self",
                "created": "2021-01-01T00:00:00Z",
                "last_updated": "2021-01-01T00:00:00Z",
                "source": "https://test.wrong.zone",
                "target": "https://example.com",
            },
            "snapshots": {
                "prechange": None,
                "postchange": None,
            },
        },
    )

    assert response.status_code == 400
    create_mapping_dependencies_mock.assert_not_called()
    destroy_mapping_dependencies_mock.assert_not_called()
