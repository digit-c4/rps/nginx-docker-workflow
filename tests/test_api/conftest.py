from unittest.mock import patch
import hashlib
import hmac

from fastapi.testclient import TestClient
import pytest

from ec_rps_workflow import settings
from ec_rps_workflow.api import app


@pytest.fixture
async def http_client():
    def sign_request(request):
        content = request.read()
        signature = hmac.new(
            key=settings.EC_RPS_SECRET.encode(),
            msg=content,
            digestmod=hashlib.sha512,
        ).hexdigest()
        request.headers["X-Hook-Signature"] = signature

    with (
        patch("ec_rps_workflow.api.webhook.setup_webhook"),
        patch("ec_rps_workflow.api.reconciliation"),
    ):
        with TestClient(app) as client:
            client._event_hooks["request"] = [sign_request]
            yield client


@pytest.fixture
async def http_client_noauth():
    with (
        patch("ec_rps_workflow.api.webhook.setup_webhook"),
        patch("ec_rps_workflow.api.reconciliation"),
    ):
        with TestClient(app) as client:
            yield client


@pytest.fixture
async def http_client_wrongauth():
    headers = {"X-Hook-Signature": "wrong-signature"}

    with (
        patch("ec_rps_workflow.api.webhook.setup_webhook"),
        patch("ec_rps_workflow.api.reconciliation"),
    ):
        with TestClient(app, headers=headers) as client:
            yield client
