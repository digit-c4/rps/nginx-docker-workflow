def test_auth(http_client):
    response = http_client.get("/health")
    assert response.status_code == 200


def test_noauth(http_client_noauth):
    response = http_client_noauth.get("/health")
    assert response.status_code == 403


def test_wrongauth(http_client_wrongauth):
    response = http_client_wrongauth.get("/health")
    assert response.status_code == 403
