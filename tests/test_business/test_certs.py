from unittest.mock import AsyncMock, MagicMock, call, patch

import pytest

from ec_rps_workflow.business.certs import certificate_cleanup, certification


@patch("ec_rps_workflow.business.mappings.extras_journal_entries_create")
@patch("ec_rps_workflow.business.certs.plugins_cert_certificate_list")
@patch("ec_rps_workflow.business.certs.plugins_cert_certificate_create")
async def test_certification_new(create_mock, list_mock, journal_mock):
    client = MagicMock()

    mapping = MagicMock()
    mapping.source = "https://test.ec.local"

    list_mock.asyncio = AsyncMock()
    list_mock.asyncio.return_value.results = []

    create_mock.asyncio = AsyncMock()

    await certification(client, mapping)

    list_mock.asyncio.assert_called_once()
    create_mock.asyncio.assert_called_once()


@patch("ec_rps_workflow.business.mappings.extras_journal_entries_create")
@patch("ec_rps_workflow.business.certs.plugins_cert_certificate_list")
@patch("ec_rps_workflow.business.certs.plugins_cert_certificate_create")
async def test_certification_existing(create_mock, list_mock, journal_mock):
    client = MagicMock()

    mapping = MagicMock()
    mapping.source = "https://test.ec.local"

    list_mock.asyncio = AsyncMock()
    list_mock.asyncio.return_value.results = [MagicMock()]

    create_mock.asyncio = AsyncMock()

    await certification(client, mapping)

    list_mock.asyncio.assert_called_once()
    create_mock.asyncio.assert_not_called()


@patch("ec_rps_workflow.business.mappings.extras_journal_entries_create")
@patch("ec_rps_workflow.business.certs.plugins_cert_certificate_list")
@patch("ec_rps_workflow.business.certs.plugins_cert_certificate_create")
async def test_certification_list_failure(create_mock, list_mock, journal_mock):
    client = MagicMock()

    mapping = MagicMock()
    mapping.source = "https://test.ec.local"

    list_mock.asyncio = AsyncMock()
    list_mock.asyncio.side_effect = RuntimeError("test")

    create_mock.asyncio = AsyncMock()

    with pytest.raises(RuntimeError, match="test"):
        await certification(client, mapping)

    assert list_mock.asyncio.call_count == 5
    create_mock.asyncio.assert_not_called()


@patch("ec_rps_workflow.business.mappings.extras_journal_entries_create")
@patch("ec_rps_workflow.business.certs.plugins_cert_certificate_list")
@patch("ec_rps_workflow.business.certs.plugins_cert_certificate_create")
async def test_certification_create_failure(create_mock, list_mock, journal_mock):
    client = MagicMock()

    mapping = MagicMock()
    mapping.source = "https://test.ec.local"

    list_mock.asyncio = AsyncMock()
    list_mock.asyncio.return_value.results = []

    create_mock.asyncio = AsyncMock()
    create_mock.asyncio.side_effect = RuntimeError("test")

    with pytest.raises(RuntimeError, match="test"):
        await certification(client, mapping)

    assert list_mock.asyncio.call_count == 5
    assert create_mock.asyncio.call_count == 5


@patch("ec_rps_workflow.business.certs.plugins_cert_certificate_list")
@patch("ec_rps_workflow.business.certs.plugins_cert_certificate_destroy")
async def test_certificate_cleanup(destroy_mock, list_mock):
    client = MagicMock()

    mapping = MagicMock()
    mapping.source = "https://test.ec.local"

    list_mock.asyncio = AsyncMock()
    list_mock.asyncio.return_value.results = [MagicMock()]

    destroy_mock.asyncio_detailed = AsyncMock()

    await certificate_cleanup(client, mapping)

    list_mock.asyncio.assert_called_once()
    destroy_mock.asyncio_detailed.assert_called_once()


@patch("ec_rps_workflow.business.certs.plugins_cert_certificate_list")
@patch("ec_rps_workflow.business.certs.plugins_cert_certificate_destroy")
async def test_certificate_cleanup_noop(destroy_mock, list_mock):
    client = MagicMock()

    mapping = MagicMock()
    mapping.source = "https://test.ec.local"

    list_mock.asyncio = AsyncMock()
    list_mock.asyncio.return_value.results = []

    destroy_mock.asyncio_detailed = AsyncMock()

    await certificate_cleanup(client, mapping)

    list_mock.asyncio.assert_called_once()
    destroy_mock.asyncio_detailed.assert_not_called()


@patch("ec_rps_workflow.business.certs.plugins_cert_certificate_list")
@patch("ec_rps_workflow.business.certs.plugins_cert_certificate_destroy")
async def test_certificate_cleanup_list_failure(destroy_mock, list_mock):
    client = MagicMock()

    mapping = MagicMock()
    mapping.source = "https://test.ec.local"

    list_mock.asyncio = AsyncMock()
    list_mock.asyncio.side_effect = RuntimeError("test")

    destroy_mock.asyncio_detailed = AsyncMock()

    with pytest.raises(RuntimeError, match="test"):
        await certificate_cleanup(client, mapping)

    assert list_mock.asyncio.call_count == 5
    destroy_mock.asyncio_detailed.assert_not_called()


@patch("ec_rps_workflow.business.certs.plugins_cert_certificate_list")
@patch("ec_rps_workflow.business.certs.plugins_cert_certificate_destroy")
async def test_certificate_cleanup_destroy_failure(destroy_mock, list_mock):
    client = MagicMock()

    mapping = MagicMock()
    mapping.source = "https://test.ec.local"

    list_mock.asyncio = AsyncMock()
    list_mock.asyncio.return_value.results = [MagicMock()]

    destroy_mock.asyncio_detailed = AsyncMock()
    destroy_mock.asyncio_detailed.side_effect = RuntimeError("test")

    with pytest.raises(RuntimeError, match="test"):
        await certificate_cleanup(client, mapping)

    assert list_mock.asyncio.call_count == 5
    assert destroy_mock.asyncio_detailed.call_count == 5
