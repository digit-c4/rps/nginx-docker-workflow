from unittest.mock import AsyncMock, MagicMock, call, patch

import pytest

from ec_rps_workflow.business import (
    create_mapping_dependencies,
    destroy_mapping_dependencies,
    reconciliation,
)


@patch("ec_rps_workflow.business.dns_registration")
@patch("ec_rps_workflow.business.certification")
async def test_create_mapping_dependencies(cert_mock, dns_mock):
    mapping = MagicMock()

    await create_mapping_dependencies(mapping)


@patch("ec_rps_workflow.business.dns_cleanup")
@patch("ec_rps_workflow.business.certificate_cleanup")
async def test_destroy_mapping_dependencies(cert_mock, dns_mock):
    mapping = MagicMock()

    await destroy_mapping_dependencies(mapping)


@patch("ec_rps_workflow.business.fetch_mappings")
@patch("ec_rps_workflow.business.create_mapping_dependencies")
async def test_reconciliation(create_mock, fetch_mock):
    mapping = MagicMock()
    mapping.source = "https://test.ec.local"

    fetch_mock.return_value = [mapping]

    await reconciliation(once=True)

    create_mock.assert_called_once()


@patch("ec_rps_workflow.business.fetch_mappings")
@patch("ec_rps_workflow.business.create_mapping_dependencies")
async def test_reconciliation_wrongzone(create_mock, fetch_mock):
    mapping = MagicMock()
    mapping.source = "https://test.wrong.zone"

    fetch_mock.return_value = [mapping]

    await reconciliation(once=True)

    create_mock.assert_not_called()


@patch("ec_rps_workflow.business.fetch_mappings")
@patch("ec_rps_workflow.business.create_mapping_dependencies")
async def test_reconciliation_silent_failure(create_mock, fetch_mock):
    mapping = MagicMock()
    mapping.source = "https://test.ec.local"

    fetch_mock.return_value = [mapping]
    create_mock.side_effect = RuntimeError("test")

    await reconciliation(once=True)

    create_mock.assert_called_once()
