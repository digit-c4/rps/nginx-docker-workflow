from unittest.mock import AsyncMock, MagicMock, call, patch

import pytest

from ec_rps_workflow.business.dns import (
    create_dns_record,
    delete_obsolete_dns_records,
    dns_cleanup,
    dns_registration,
    find_dns_zone,
    find_vips,
    wait_for_lookup,
)


@patch("ec_rps_workflow.business.dns.plugins_netbox_dns_zones_list")
async def test_find_dns_zone(mocked_api):
    zone = MagicMock()
    zone.id = 1
    zone.__eq__.side_effect = lambda other: other.id == 1

    mocked_api.asyncio = AsyncMock()
    mocked_api.asyncio.return_value.results = [zone]
    client = MagicMock()

    got = await find_dns_zone(client)
    assert got == zone


@patch("ec_rps_workflow.business.dns.plugins_netbox_dns_zones_list")
async def test_find_dns_zone_noresult(mocked_api):
    mocked_api.asyncio = AsyncMock()
    mocked_api.asyncio.return_value.results = []
    client = MagicMock()

    with pytest.raises(RuntimeError):
        await find_dns_zone(client)


@patch("ec_rps_workflow.business.dns.plugins_netbox_dns_zones_list")
async def test_find_dns_zone_failure(mocked_api):
    mocked_api.asyncio = AsyncMock()
    mocked_api.asyncio.side_effect = RuntimeError("test")
    client = MagicMock()

    with pytest.raises(RuntimeError, match="test"):
        await find_dns_zone(client)


@patch("ec_rps_workflow.business.dns.ipam_ip_addresses_list")
async def test_find_vips(mocked_api):
    ip = MagicMock()
    ip.id = 1
    ip.__eq__.side_effect = lambda other: other.id == 1

    mocked_api.asyncio = AsyncMock()
    mocked_api.asyncio.return_value.results = [ip]
    client = MagicMock()

    res = await find_vips(client)
    assert len(res) == 1
    assert res[0] == ip


@patch("ec_rps_workflow.business.dns.ipam_ip_addresses_list")
async def test_find_vips_noresult(mocked_api):
    mocked_api.asyncio = AsyncMock()
    mocked_api.asyncio.return_value.results = []
    client = MagicMock()

    with pytest.raises(RuntimeError):
        await find_vips(client)


@patch("ec_rps_workflow.business.dns.ipam_ip_addresses_list")
async def test_find_vips_failure(mocked_api):
    mocked_api.asyncio = AsyncMock()
    mocked_api.asyncio.side_effect = RuntimeError("test")
    client = MagicMock()

    with pytest.raises(RuntimeError, match="test"):
        await find_vips(client)


@patch("ec_rps_workflow.business.dns.plugins_netbox_dns_records_list")
@patch("ec_rps_workflow.business.dns.plugins_netbox_dns_records_create")
async def test_create_dns_record_exists(create_mock, list_mock):
    client = MagicMock()

    zone = MagicMock()
    zone.name = "ec.local"

    ip = MagicMock()
    ip.address = "1.2.3.4"

    record = MagicMock()
    record.id = 1
    record.__eq__.side_effect = lambda other: other.id == 1

    create_mock.asyncio = AsyncMock()

    list_mock.asyncio = AsyncMock()
    list_mock.asyncio.return_value.results = [record]

    await create_dns_record(client, zone, "test", ip)

    list_mock.asyncio.assert_called_once()
    create_mock.asyncio.assert_not_called()


@patch("ec_rps_workflow.business.dns.plugins_netbox_dns_records_list")
@patch("ec_rps_workflow.business.dns.plugins_netbox_dns_records_create")
async def test_create_dns_record_missing(create_mock, list_mock):
    client = MagicMock()

    zone = MagicMock()
    zone.name = "ec.local"

    ip = MagicMock()
    ip.address = "1.2.3.4"

    record = MagicMock()
    record.id = 1
    record.__eq__.side_effect = lambda other: other.id == 1

    create_mock.asyncio = AsyncMock()

    list_mock.asyncio = AsyncMock()
    list_mock.asyncio.return_value.results = []

    await create_dns_record(client, zone, "test", ip)

    list_mock.asyncio.assert_called_once()
    create_mock.asyncio.assert_called_once()


@patch("ec_rps_workflow.business.dns.plugins_netbox_dns_records_list")
@patch("ec_rps_workflow.business.dns.plugins_netbox_dns_records_create")
async def test_create_dns_record_list_failure(create_mock, list_mock):
    client = MagicMock()

    zone = MagicMock()
    zone.name = "ec.local"

    ip = MagicMock()
    ip.address = "1.2.3.4"

    list_mock.asyncio = AsyncMock()
    list_mock.asyncio.side_effect = RuntimeError("test")

    create_mock.asyncio = AsyncMock()

    with pytest.raises(RuntimeError, match="test"):
        await create_dns_record(client, zone, "test", ip)

    assert list_mock.asyncio.call_count == 5
    create_mock.asyncio.assert_not_called()


@patch("ec_rps_workflow.business.dns.plugins_netbox_dns_records_list")
@patch("ec_rps_workflow.business.dns.plugins_netbox_dns_records_create")
async def test_create_dns_record_create_failure(create_mock, list_mock):
    client = MagicMock()

    zone = MagicMock()
    zone.name = "ec.local"

    ip = MagicMock()
    ip.address = "1.2.3.4"

    list_mock.asyncio = AsyncMock()
    list_mock.asyncio.return_value.results = []

    create_mock.asyncio = AsyncMock()
    create_mock.asyncio.side_effect = RuntimeError("test")

    with pytest.raises(RuntimeError, match="test"):
        await create_dns_record(client, zone, "test", ip)

    assert list_mock.asyncio.call_count == 5
    assert create_mock.asyncio.call_count == 5


@patch("ec_rps_workflow.business.dns.plugins_netbox_dns_records_list")
@patch("ec_rps_workflow.business.dns.plugins_netbox_dns_records_destroy")
async def test_delete_obsolete_dns_records(delete_mock, list_mock):
    client = MagicMock()

    zone = MagicMock()
    zone.name = "ec.local"

    ip = MagicMock()
    ip.address = "1.2.3.4"

    record = MagicMock()
    record.id = 1
    record.__eq__.side_effect = lambda other: other.id == 1

    delete_mock.asyncio_detailed = AsyncMock()

    list_mock.asyncio = AsyncMock()
    list_mock.asyncio.return_value.results = [record]

    await delete_obsolete_dns_records(client, zone, "test", [ip])

    list_mock.asyncio.assert_called_once()
    delete_mock.asyncio_detailed.assert_has_calls(
        [call(client=client, id=1)],
        any_order=True,
    )


@patch("ec_rps_workflow.business.dns.plugins_netbox_dns_records_list")
@patch("ec_rps_workflow.business.dns.plugins_netbox_dns_records_destroy")
async def test_delete_obsolete_dns_records_noop(delete_mock, list_mock):
    client = MagicMock()

    zone = MagicMock()
    zone.name = "ec.local"

    ip = MagicMock()
    ip.address = "1.2.3.4"

    delete_mock.asyncio_detailed = AsyncMock()

    list_mock.asyncio = AsyncMock()
    list_mock.asyncio.return_value.results = []

    await delete_obsolete_dns_records(client, zone, "test", [ip])

    list_mock.asyncio.assert_called_once()
    delete_mock.asyncio_detailed.assert_not_called()


@patch("ec_rps_workflow.business.dns.plugins_netbox_dns_records_list")
@patch("ec_rps_workflow.business.dns.plugins_netbox_dns_records_destroy")
async def test_delete_obsolete_dns_records_list_failure(delete_mock, list_mock):
    client = MagicMock()

    zone = MagicMock()
    zone.name = "ec.local"

    ip = MagicMock()
    ip.address = "1.2.3.4"

    record = MagicMock()
    record.id = 1
    record.__eq__.side_effect = lambda other: other.id == 1

    delete_mock.asyncio_detailed = AsyncMock()

    list_mock.asyncio = AsyncMock()
    list_mock.asyncio.side_effect = RuntimeError("test")

    with pytest.raises(RuntimeError, match="test"):
        await delete_obsolete_dns_records(client, zone, "test", [ip])

    assert list_mock.asyncio.call_count == 5
    delete_mock.asyncio_detailed.assert_not_called()


@patch("ec_rps_workflow.business.dns.plugins_netbox_dns_records_list")
@patch("ec_rps_workflow.business.dns.plugins_netbox_dns_records_destroy")
async def test_delete_obsolete_dns_records_destroy_failure(delete_mock, list_mock):
    client = MagicMock()

    zone = MagicMock()
    zone.name = "ec.local"

    ip = MagicMock()
    ip.address = "1.2.3.4"

    record = MagicMock()
    record.id = 1
    record.__eq__.side_effect = lambda other: other.id == 1

    delete_mock.asyncio_detailed = AsyncMock()
    delete_mock.asyncio_detailed.side_effect = RuntimeError("test")

    list_mock.asyncio = AsyncMock()
    list_mock.asyncio.return_value.results = [record]

    with pytest.raises(RuntimeError, match="test"):
        await delete_obsolete_dns_records(client, zone, "test", [ip])

    assert list_mock.asyncio.call_count == 5
    assert delete_mock.asyncio_detailed.call_count == 5


@patch("ec_rps_workflow.business.dns.anyio.getaddrinfo")
async def test_wait_for_lookup(getaddrinfo_mock):

    ip = MagicMock()
    ip.address = "1.2.3.4"

    gen = wait_for_lookup("test.ec.local", [ip])

    getaddrinfo_mock.return_value = []
    await gen.__anext__()

    getaddrinfo_mock.return_value = [[["1.2.3.4"]]]
    with pytest.raises(StopAsyncIteration):
        await gen.__anext__()


@patch("ec_rps_workflow.business.dns.find_dns_zone")
@patch("ec_rps_workflow.business.dns.find_vips")
@patch("ec_rps_workflow.business.dns.create_dns_record")
@patch("ec_rps_workflow.business.dns.delete_obsolete_dns_records")
@patch("ec_rps_workflow.business.dns.wait_for_lookup")
@patch("ec_rps_workflow.business.mappings.extras_journal_entries_create")
async def test_dns_registration(
    journal_mock,
    wait_mock,
    delete_mock,
    create_mock,
    find_vips_mock,
    find_dns_zone_mock,
):
    client = MagicMock()
    mapping = MagicMock()
    mapping.source = "https://test.ec.local"

    zone = MagicMock()
    ip = MagicMock()

    journal_mock.asyncio = AsyncMock()

    async def wait_mock_impl(*_args, **_kwargs):
        yield

    wait_mock.side_effect = wait_mock_impl

    find_vips_mock.return_value = [ip]
    find_dns_zone_mock.return_value = zone

    await dns_registration(client, mapping)

    find_dns_zone_mock.assert_called_once()
    find_vips_mock.assert_called_once()
    create_mock.assert_called_once()
    delete_mock.assert_called_once()
    wait_mock.assert_called_once()


@patch("ec_rps_workflow.business.dns.find_dns_zone")
@patch("ec_rps_workflow.business.dns.find_vips")
@patch("ec_rps_workflow.business.dns.create_dns_record")
@patch("ec_rps_workflow.business.dns.delete_obsolete_dns_records")
@patch("ec_rps_workflow.business.dns.wait_for_lookup")
@patch("ec_rps_workflow.business.mappings.extras_journal_entries_create")
async def test_dns_registration_timeout(
    journal_mock,
    wait_mock,
    delete_mock,
    create_mock,
    find_vips_mock,
    find_dns_zone_mock,
):
    client = MagicMock()
    mapping = MagicMock()
    mapping.source = "https://test.ec.local"

    zone = MagicMock()
    ip = MagicMock()

    journal_mock.asyncio = AsyncMock()

    async def wait_mock_impl(*_args, **_kwargs):
        while True:
            yield

    wait_mock.side_effect = wait_mock_impl

    find_vips_mock.return_value = [ip]
    find_dns_zone_mock.return_value = zone

    with pytest.raises(RuntimeError):
        await dns_registration(client, mapping)

    find_dns_zone_mock.assert_called_once()
    find_vips_mock.assert_called_once()
    create_mock.assert_called_once()
    delete_mock.assert_called_once()
    wait_mock.assert_called_once()


@patch("ec_rps_workflow.business.dns.find_dns_zone")
@patch("ec_rps_workflow.business.dns.plugins_netbox_dns_records_list")
@patch("ec_rps_workflow.business.dns.plugins_netbox_dns_records_destroy")
async def test_dns_cleanup(delete_mock, list_mock, find_dns_zone_mock):
    client = MagicMock()
    mapping = MagicMock()
    mapping.source = "https://test.ec.local"

    zone = MagicMock()
    zone.name = "ec.local"

    record = MagicMock()
    record.id = 1

    find_dns_zone_mock.return_value = zone

    list_mock.asyncio = AsyncMock()
    list_mock.asyncio.return_value.results = [record]

    delete_mock.asyncio_detailed = AsyncMock()

    await dns_cleanup(client, mapping)

    list_mock.asyncio.assert_called_once()
    delete_mock.asyncio_detailed.assert_called_once()
