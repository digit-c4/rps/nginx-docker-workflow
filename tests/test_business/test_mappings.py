from unittest.mock import AsyncMock, MagicMock, patch

import pytest

from ec_rps_workflow.business.mappings import fetch_mappings


@patch("ec_rps_workflow.business.mappings.plugins_rps_mapping_list")
async def test_fetch_mappings(mocked_api):
    mapping = MagicMock()
    mapping.id = 1
    mapping.__eq__.side_effect = lambda other: other.id == 1

    mocked_api.asyncio = AsyncMock()
    mocked_api.asyncio.return_value.results = [mapping]

    res = await fetch_mappings()
    assert len(res) == 1
    assert res[0] == mapping

    mocked_api.asyncio.assert_called_once()
    assert any(
        call[1].get("tag") == ["test"] for call in mocked_api.asyncio.call_args_list
    ), mocked_api.asyncio.call_args_list


@patch("ec_rps_workflow.business.mappings.plugins_rps_mapping_list")
async def test_fetch_mappings_failure(mocked_api):
    mocked_api.asyncio = AsyncMock()
    mocked_api.asyncio.side_effect = RuntimeError("test")

    with pytest.raises(RuntimeError, match="test"):
        await fetch_mappings()

    assert mocked_api.asyncio.call_count == 5
