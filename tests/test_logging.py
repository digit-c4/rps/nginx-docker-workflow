from unittest.mock import AsyncMock, MagicMock

from freezegun import freeze_time
from logbook import Handler, Logger
from logbook import TestHandler as LogbookTestHandler
from logbook import set_datetime_format
import pytest

from ec_rps_workflow import logging


@pytest.fixture
def loghandler():
    handler = LogbookTestHandler()
    handler.formatter = logging.logfmt_formatter

    with freeze_time("2021-01-01 12:00:00") as frozen_datetime:
        set_datetime_format(frozen_datetime)

        with handler.applicationbound():
            yield handler


@pytest.fixture
def logger(loghandler):
    yield Logger("test")


def test_logfmt(logger, loghandler):
    logger.info("Hello, world!")
    assert (
        loghandler.formatted_records[0]
        == 'app="ec_rps_workflow" bubble="test" time="2021-01-01 12:00:00" level="INFO" message="Hello, world!"'
    )


def test_log_exception(logger, loghandler):
    try:
        raise RuntimeError("test")

    except RuntimeError as err:
        logger.exception("error")

    assert 'exc.type="RuntimeError"' in loghandler.formatted_records[0]
    assert 'exc.message="test"' in loghandler.formatted_records[0]
    assert 'message="error"' in loghandler.formatted_records[0]


def test_create_loghandler():
    handler = logging.create_loghandler()
    assert isinstance(handler, Handler)


async def test_accesslog(loghandler):
    app = MagicMock()

    request = MagicMock()
    request.method = "GET"
    request.url.path = "/"
    request.client.host = "1.2.3.4"
    request.client.port = 32444

    call_next = AsyncMock()
    call_next.return_value.status_code = 200

    middleware = logging.Middleware(app)

    resp = await middleware.dispatch(request, call_next)
    assert resp.status_code == 200

    assert (
        loghandler.formatted_records[0]
        == 'app="ec_rps_workflow" bubble="test" time="2021-01-01 12:00:00" level="INFO" message="Incomming Request" request.method="GET" request.path="/" response.status_code=200 request.client="1.2.3.4:32444"'
    )
