# NGINX Docker Workflow

**WIP**

## TODO

 - [x] :construction: Draft implementation
 - [x] :white_check_mark: Unit Test suite
 - [ ] :memo: Documentation
 - [x] :whale: Docker image
 - [x] :construction_worker: Gitlab CI pipeline to build and push Docker image, and publish documentation
