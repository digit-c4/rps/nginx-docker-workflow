"""
This module provides a structured log formatter, using the logfmt format.
"""

from typing import Any, Awaitable, Callable

import sys
import traceback

from logbook import Handler, Logger, LogRecord, StreamHandler
from starlette.middleware.base import BaseHTTPMiddleware
from starlette.requests import Request
from starlette.responses import Response
import logfmt

from ec_rps_workflow import settings


def logfmt_formatter(record: LogRecord, _handler: Handler) -> str:
    """
    Logbook log formatter.
    """

    logentry = {
        "app": "ec_rps_workflow",
        "bubble": settings.EC_BUBBLE,
        "time": record.time,
        "level": record.level_name,
        "message": record.message,
    }
    logentry.update(record.extra)

    if record.exc_info:
        exc_type, exc, tb = record.exc_info
        logentry["exc.type"] = f"{exc_type.__name__}"
        logentry["exc.message"] = f"{exc}"
        traceback.print_exception(exc_type, exc, tb, file=sys.stderr)

    logentry.pop("color_message", None)
    logoutput: str = logfmt.format_line(logentry)
    return logoutput


def create_loghandler() -> Handler:
    """
    Logbook handler factory.
    """

    handler = StreamHandler(sys.stdout, level="INFO")
    handler.formatter = logfmt_formatter
    return handler


class Middleware(BaseHTTPMiddleware):
    def __init__(self, *args: Any, **kwargs: Any):
        super().__init__(*args, **kwargs)

        self._logger = Logger("ec_rps_workflow.access")

    async def dispatch(
        self,
        request: Request,
        call_next: Callable[[Request], Awaitable[Response]],
    ) -> Response:
        response = await call_next(request)

        extra = {
            "request.method": request.method,
            "request.path": request.url.path,
            "response.status_code": response.status_code,
        }

        if request.client is not None:
            extra["request.client"] = f"{request.client.host}:{request.client.port}"

        self._logger.info("Incomming Request", extra=extra)

        return response
