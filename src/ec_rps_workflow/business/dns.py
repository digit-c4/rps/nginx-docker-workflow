"""
Business logic for DNS record management.
"""

from typing import TYPE_CHECKING, AsyncGenerator

from urllib.parse import urlsplit

from tenacity import retry, stop_after_attempt, wait_random_exponential
import anyio

from ec_netbox import ECNetboxClient
from ec_netbox_openapi.api.ipam import ipam_ip_addresses_list
from ec_netbox_openapi.api.plugins import (
    plugins_netbox_dns_records_create,
    plugins_netbox_dns_records_destroy,
    plugins_netbox_dns_records_list,
    plugins_netbox_dns_zones_list,
)
from ec_netbox_openapi.models import (
    IPAddress,
    Mapping,
    WritableJournalEntryRequestKind,
    WritableRecordRequest,
    WritableRecordRequestType,
    Zone,
)
from ec_rps_workflow import settings

from .mappings import create_mapping_journal_entry, mapping_journalling


@retry(
    reraise=True,
    stop=stop_after_attempt(5),
    wait=wait_random_exponential(
        multiplier=settings.RETRY_WAIT_MULTIPLIER,
        max=settings.RETRY_WAIT_MAX,
    ),
    sleep=anyio.sleep,
)
async def find_dns_zone(client: ECNetboxClient) -> Zone:
    """
    Find the DNS zone for the RPS service.
    """

    res = await plugins_netbox_dns_zones_list.asyncio(
        client=client,
        name=[settings.EC_RPS_DNS_ZONE],
        view=[settings.EC_RPS_DNS_VIEW],
    )

    if TYPE_CHECKING:
        assert res is not None, "should raise on unexpected status"
        assert isinstance(res.results, list), "never unset when in response"

    if len(res.results) < 1:
        raise RuntimeError("DNS zone not found")

    return res.results[0]


@retry(
    reraise=True,
    stop=stop_after_attempt(5),
    wait=wait_random_exponential(
        multiplier=settings.RETRY_WAIT_MULTIPLIER,
        max=settings.RETRY_WAIT_MAX,
    ),
    sleep=anyio.sleep,
)
async def find_vips(client: ECNetboxClient) -> list[IPAddress]:
    """
    Find the VIPs for the RPS service.
    """

    res = await ipam_ip_addresses_list.asyncio(
        client=client,
        role=["vip"],
        tag=[f"vip_{settings.EC_RPS_DNS_VIEW.lower()}"],
    )

    if TYPE_CHECKING:
        assert res is not None, "should raise on unexpected status"
        assert isinstance(res.results, list), "never unset when in response"

    if len(res.results) < 1:
        raise RuntimeError("VIP not found")

    return res.results


@retry(
    reraise=True,
    stop=stop_after_attempt(5),
    wait=wait_random_exponential(
        multiplier=settings.RETRY_WAIT_MULTIPLIER,
        max=settings.RETRY_WAIT_MAX,
    ),
    sleep=anyio.sleep,
)
async def create_dns_record(
    client: ECNetboxClient,
    zone: Zone,
    record_name: str,
    vip: IPAddress,
) -> None:
    """
    Create a DNS record in the Netbox API.
    """

    res = await plugins_netbox_dns_records_list.asyncio(
        client=client,
        zone=[zone.name],
        name=[record_name],
        type=["A"],
        value=[vip.address],
    )

    if TYPE_CHECKING:
        assert res is not None, "should raise on unexpected status"
        assert isinstance(res.results, list), "never unset when in response"

    if len(res.results) == 0:
        await plugins_netbox_dns_records_create.asyncio(
            client=client,
            body=WritableRecordRequest(
                zone=zone.id,
                name=record_name,
                type=WritableRecordRequestType.A,
                value=vip.address,
            ),
        )


@retry(
    reraise=True,
    stop=stop_after_attempt(5),
    wait=wait_random_exponential(
        multiplier=settings.RETRY_WAIT_MULTIPLIER,
        max=settings.RETRY_WAIT_MAX,
    ),
    sleep=anyio.sleep,
)
async def delete_obsolete_dns_records(
    client: ECNetboxClient,
    zone: Zone,
    record_name: str,
    vips: list[IPAddress],
) -> None:
    """
    Remove all DNS records that do not match the current VIPs.
    """

    res = await plugins_netbox_dns_records_list.asyncio(
        client=client,
        zone=[zone.name],
        name=[record_name],
        type=["A"],
        value_n=[vip.address for vip in vips],
    )

    if TYPE_CHECKING:
        assert res is not None, "should raise on unexpected status"
        assert isinstance(res.results, list), "never unset when in response"

    for record in res.results:
        await plugins_netbox_dns_records_destroy.asyncio_detailed(
            client=client,
            id=record.id,
        )


async def wait_for_lookup(
    hostname: str,
    vips: list[IPAddress],
) -> AsyncGenerator[None, None]:
    """
    Wait for a DNS lookup to return the expected VIPs.
    """

    while True:
        nslookup = await anyio.getaddrinfo(hostname, None)

        if len(nslookup) > 0 and any(
            all(addr[-1][0] == vip.address for addr in nslookup) for vip in vips
        ):
            return

        yield


async def dns_registration(client: ECNetboxClient, mapping: Mapping) -> None:
    """
    Create a DNS record for a mapping (for each VIP)
    """

    phase = "dns registration"

    url = urlsplit(mapping.source)

    if TYPE_CHECKING:
        assert url.hostname is not None, "already verified in caller"

    record_name = url.hostname[: -len(settings.EC_RPS_DNS_ZONE) - 1]

    async with mapping_journalling(client, mapping, phase):
        zone = await find_dns_zone(client)
        vips = await find_vips(client)

        async with anyio.create_task_group() as tg:
            for vip in vips:
                tg.start_soon(
                    create_dns_record,
                    client,
                    zone,
                    record_name,
                    vip,
                )

            tg.start_soon(
                delete_obsolete_dns_records,
                client,
                zone,
                record_name,
                vips,
            )

        try:
            with anyio.fail_after(settings.DNS_LOOKUP_TIMEOUT):
                async for _ in wait_for_lookup(url.hostname, vips):
                    await create_mapping_journal_entry(
                        client,
                        mapping,
                        WritableJournalEntryRequestKind.WARNING,
                        f"{phase} waiting for resolution (retry in 5min)",
                    )
                    await anyio.sleep(settings.DNS_LOOKUP_RETRY_INTERVAL)

        except TimeoutError:
            raise RuntimeError("dns resolution timeout")


@retry(
    reraise=True,
    stop=stop_after_attempt(5),
    wait=wait_random_exponential(
        multiplier=settings.RETRY_WAIT_MULTIPLIER,
        max=settings.RETRY_WAIT_MAX,
    ),
    sleep=anyio.sleep,
)
async def dns_cleanup(client: ECNetboxClient, mapping: Mapping) -> None:
    """
    Remove DNS records related to a mapping
    """

    url = urlsplit(mapping.source)

    if TYPE_CHECKING:
        assert url.hostname is not None, "already verified in caller"

    record_name = url.hostname[: -len(settings.EC_RPS_DNS_ZONE) - 1]
    zone = await find_dns_zone(client)

    res = await plugins_netbox_dns_records_list.asyncio(
        client=client,
        zone=[zone.name],
        name=[record_name],
        type=["A"],
    )

    if TYPE_CHECKING:
        assert res is not None, "should raise on unexpected status"
        assert isinstance(res.results, list), "never unset when in response"

    for record in res.results:
        await plugins_netbox_dns_records_destroy.asyncio_detailed(
            client=client,
            id=record.id,
        )
