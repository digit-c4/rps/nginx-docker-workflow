"""
Business logic for certificate management.
"""

from typing import TYPE_CHECKING

from urllib.parse import urlsplit

from tenacity import retry, stop_after_attempt, wait_random_exponential
import anyio

from ec_netbox import ECNetboxClient
from ec_netbox_openapi.api.plugins import (
    plugins_cert_certificate_create,
    plugins_cert_certificate_destroy,
    plugins_cert_certificate_list,
)
from ec_netbox_openapi.models import (
    CertificateRequest,
    CertificateRequestCertificateAuthority,
    Mapping,
)
from ec_rps_workflow import settings

from .mappings import mapping_journalling


@retry(
    reraise=True,
    stop=stop_after_attempt(5),
    wait=wait_random_exponential(
        multiplier=settings.RETRY_WAIT_MULTIPLIER,
        max=settings.RETRY_WAIT_MAX,
    ),
    sleep=anyio.sleep,
)
async def certification(client: ECNetboxClient, mapping: Mapping) -> None:
    """
    Create certificate for a mapping.
    """

    async with mapping_journalling(client, mapping, "certification"):
        url = urlsplit(mapping.source)

        if TYPE_CHECKING:
            assert url.hostname is not None, "already verified in caller"

        res = await plugins_cert_certificate_list.asyncio(
            client=client,
            cn=[url.hostname],
        )

        if TYPE_CHECKING:
            assert res is not None, "should raise on unexpected status"
            assert isinstance(res.results, list), "never unset when in response"

        if len(res.results) == 0:
            await plugins_cert_certificate_create.asyncio(
                client=client,
                body=CertificateRequest(
                    cn=url.hostname,
                    ca=CertificateRequestCertificateAuthority(settings.EC_RPS_ACME_CA),
                ),
            )


@retry(
    reraise=True,
    stop=stop_after_attempt(5),
    wait=wait_random_exponential(
        multiplier=settings.RETRY_WAIT_MULTIPLIER,
        max=settings.RETRY_WAIT_MAX,
    ),
    sleep=anyio.sleep,
)
async def certificate_cleanup(client: ECNetboxClient, mapping: Mapping) -> None:
    """
    Destroy certificate for a mapping.
    """

    url = urlsplit(mapping.source)

    if TYPE_CHECKING:
        assert url.hostname is not None, "already verified in caller"

    res = await plugins_cert_certificate_list.asyncio(
        client=client,
        cn=[url.hostname],
    )

    if TYPE_CHECKING:
        assert res is not None, "should raise on unexpected status"
        assert isinstance(res.results, list), "never unset when in response"

    for cert in res.results:
        await plugins_cert_certificate_destroy.asyncio_detailed(
            client=client,
            id=cert.id,
        )
