"""
Business logic for mappings lifecycle.
"""

from typing import TYPE_CHECKING, AsyncGenerator

from contextlib import asynccontextmanager

from logbook import Logger
from tenacity import retry, stop_after_attempt, wait_random_exponential
import anyio

from ec_netbox import ECNetboxClient
from ec_netbox_openapi.api.extras import extras_journal_entries_create
from ec_netbox_openapi.api.plugins import plugins_rps_mapping_list
from ec_netbox_openapi.models import (
    Mapping,
    WritableJournalEntryRequest,
    WritableJournalEntryRequestKind,
)
from ec_rps_workflow import settings

logger = Logger(__name__)


@retry(
    retry_error_callback=lambda *_: None,
    stop=stop_after_attempt(5),
    wait=wait_random_exponential(
        multiplier=settings.RETRY_WAIT_MULTIPLIER,
        max=settings.RETRY_WAIT_MAX,
    ),
    sleep=anyio.sleep,
)
async def create_mapping_journal_entry(
    client: ECNetboxClient,
    mapping: Mapping,
    kind: WritableJournalEntryRequestKind,
    comment: str,
) -> None:
    """
    Create a journal entry for a mapping in the Netbox API.
    """

    await extras_journal_entries_create.asyncio(
        client=client,
        body=WritableJournalEntryRequest(
            assigned_object_id=mapping.id,
            assigned_object_type="netbox_rps_plugin.mapping",
            kind=kind,
            comments=comment,
        ),
    )


@asynccontextmanager
async def mapping_journalling(
    client: ECNetboxClient,
    mapping: Mapping,
    phase: str,
) -> AsyncGenerator[None, None]:
    """
    Wrapper for a block of code that interacts with a mapping, logging via
    journal entries the progress.
    """

    await create_mapping_journal_entry(
        client,
        mapping,
        WritableJournalEntryRequestKind.INFO,
        f"{phase.capitalize()} in progress",
    )

    try:
        yield

    except Exception as err:
        await create_mapping_journal_entry(
            client,
            mapping,
            WritableJournalEntryRequestKind.DANGER,
            f"{phase.capitalize()} failed: {err}",
        )
        raise

    else:
        await create_mapping_journal_entry(
            client,
            mapping,
            WritableJournalEntryRequestKind.SUCCESS,
            f"{phase.capitalize()} successful",
        )


@retry(
    reraise=True,
    stop=stop_after_attempt(5),
    wait=wait_random_exponential(
        multiplier=settings.RETRY_WAIT_MULTIPLIER,
        max=settings.RETRY_WAIT_MAX,
    ),
    sleep=anyio.sleep,
)
async def fetch_mappings() -> list[Mapping]:
    """
    Fetch all mappings from the Netbox API.
    """

    client = ECNetboxClient(
        base_url=settings.NETBOX_API,
        token=settings.NETBOX_TOKEN,
        verify_ssl=False,
        raise_on_unexpected_status=True,
        logger=logger,
    )

    async with client as client:
        res = await plugins_rps_mapping_list.asyncio(
            client=client,
            tag=[settings.EC_ENV],
        )

        if TYPE_CHECKING:
            assert res is not None, "should raise on unexpected status"
            assert isinstance(res.results, list), "never unset when in response"

        return res.results
