"""
High-level business logic for creating and destroying mapping dependencies.
"""

from urllib.parse import urlsplit

from logbook import Logger
import anyio

from ec_netbox import ECNetboxClient
from ec_netbox_openapi.models import Mapping
from ec_rps_workflow import settings

from .certs import certificate_cleanup, certification
from .dns import dns_cleanup, dns_registration
from .mappings import fetch_mappings

logger = Logger(__name__)


async def create_mapping_dependencies(mapping: Mapping) -> None:
    """
    Create DNS records and Certificate for a mapping
    """

    client = ECNetboxClient(
        base_url=settings.NETBOX_API,
        token=settings.NETBOX_TOKEN,
        verify_ssl=False,
        raise_on_unexpected_status=True,
        logger=logger,
    )

    async with client as client:
        await dns_registration(client, mapping)
        await certification(client, mapping)


async def destroy_mapping_dependencies(mapping: Mapping) -> None:
    """
    Destroy DNS records and Certificate for a mapping
    """

    client = ECNetboxClient(
        base_url=settings.NETBOX_API,
        token=settings.NETBOX_TOKEN,
        verify_ssl=False,
        raise_on_unexpected_status=True,
        logger=logger,
    )

    async with client as client:
        await dns_cleanup(client, mapping)
        await certificate_cleanup(client, mapping)


async def reconciliation(once: bool = False) -> None:
    """
    Ensure existing mappings have their dependencies created.
    """

    while True:
        mappings = await fetch_mappings()

        for mapping in mappings:
            url = urlsplit(mapping.source)

            if url.hostname is None or not url.hostname.endswith(
                f".{settings.EC_RPS_DNS_ZONE}"
            ):
                err = "Mapping's source URL is not in the expected DNS zone"
                logger.error(
                    err,
                    extra={
                        "mapping": mapping.source,
                        "zone": settings.EC_RPS_DNS_ZONE,
                    },
                )

            else:
                try:
                    await create_mapping_dependencies(mapping)

                except Exception as err:
                    logger.exception(f"{err}")

        await anyio.sleep(settings.RECONCILIATION_INTERVAL)

        if once:
            break
