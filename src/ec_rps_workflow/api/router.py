"""
This module provides the API routes for the Workflow Controller.
"""

from typing import Any, Literal, Optional

from urllib.parse import urlsplit

from fastapi import APIRouter, HTTPException, Request
from logbook import Logger
from pydantic import BaseModel

from ec_netbox_openapi.models import Mapping
from ec_rps_workflow import business, settings


class WebhookPayloadSnapshots(BaseModel):
    prechange: Optional[dict[str, Any]]
    postchange: Optional[dict[str, Any]]


class WebhookPayload(BaseModel):
    event: Literal["created", "updated", "deleted"]
    timestamp: str
    model: str
    username: str
    request_id: str
    data: dict[str, Any]
    snapshots: WebhookPayloadSnapshots


class WebhookResponse(BaseModel):
    success: bool


class HealthcheckResponse(BaseModel):
    status: Literal["ok", "error"]
    detail: Optional[str]


router = APIRouter()
logger = Logger(__name__)


@router.get("/health")
async def healthcheck() -> HealthcheckResponse:
    """
    Always returns 200 OK.
    """

    return HealthcheckResponse(status="ok", detail=None)


@router.post("/webhook/netbox")
async def webhook_netbox(
    request: Request,
    payload: WebhookPayload,
) -> WebhookResponse:
    """
    Endpoint for receiving Netbox webhooks.
    """

    if payload.model == "mapping":
        mapping = Mapping.from_dict(payload.data)
        url = urlsplit(mapping.source)

        if url.hostname is None or not url.hostname.endswith(
            f".{settings.EC_RPS_DNS_ZONE}"
        ):
            err = "Mapping's source URL is not in the expected DNS zone"
            logger.error(
                err,
                extra={
                    "mapping": mapping.source,
                    "zone": settings.EC_RPS_DNS_ZONE,
                },
            )
            raise HTTPException(
                status_code=400,
                detail=err,
            )

        try:
            match payload.event:
                case "created" | "updated":
                    await business.create_mapping_dependencies(mapping)

                case "deleted":
                    await business.destroy_mapping_dependencies(mapping)

        except Exception as err:
            logger.exception(f"{err}")
            raise HTTPException(status_code=500, detail=f"{err}")

    return WebhookResponse(success=True)
