"""
This module contains the API server for the Workflow Controller. It is
responsible for receiving updates from the Netbox webhook.
"""

from typing import AsyncGenerator

from contextlib import asynccontextmanager
import hashlib
import hmac

from fastapi import Depends, FastAPI, HTTPException, Request
import anyio

from ec_rps_workflow import logging, settings
from ec_rps_workflow.business import reconciliation, webhook

from .router import router


@asynccontextmanager
async def lifespan(app: FastAPI) -> AsyncGenerator[None, None]:
    """
    Life cycle of the web server.
    """

    await webhook.setup_webhook()

    async with anyio.create_task_group() as tg:
        tg.start_soon(reconciliation)

        try:
            yield

        finally:
            tg.cancel_scope.cancel()


async def verify_request_signature(request: Request) -> None:
    """
    Netbox hashes the request body with the secret key using HMAC-SHA512 and
    sends the hash in the X-Hook-Signature header. This function verifies the
    signature to ensure the request is authentic.

    :raises HTTPException:
        If the signature is invalid, a 403 exception is raised.
    """

    content = await request.body()
    digest = hmac.new(
        key=settings.EC_RPS_SECRET.encode(),
        msg=content,
        digestmod=hashlib.sha512,
    ).hexdigest()

    signature = request.headers.get("X-Hook-Signature")
    if signature != digest:
        raise HTTPException(status_code=403, detail="Invalid request signature")


app = FastAPI(
    title="EC Workflow API",
    version="0.1.0",
    lifespan=lifespan,
    dependencies=[Depends(verify_request_signature)],
)
app.add_middleware(logging.Middleware)
app.include_router(router)
