"""
Launcher for the web server.
"""

from logging import getLogger

from logbook.compat import redirect_logging
import uvicorn

from ec_rps_workflow import api, logging, settings

with logging.create_loghandler().applicationbound():
    httpx_logger = getLogger("httpx")
    httpx_logger.disabled = True

    redirect_logging()

    try:
        uvicorn.run(
            api.app,
            port=settings.BIND_PORT,
            log_config=None,
            access_log=False,
            use_colors=False,
        )

    except KeyboardInterrupt:
        pass
