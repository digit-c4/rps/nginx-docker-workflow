from typing import TYPE_CHECKING

import sys

from decouple import config

_settings_module: str = config(
    "EC_RPS_SETTINGS_MODULE",
    default="ec_rps_workflow.settings.prod",
)

try:
    __import__(_settings_module)

except ImportError as err:
    print(err, file=sys.stderr)
    sys.exit(1)

else:
    _mod = sys.modules[_settings_module]
    locals().update(_mod.__dict__)

if TYPE_CHECKING:
    DEBUG: bool
    BIND_PORT: int

    NETBOX_API: str
    NETBOX_TOKEN: str

    EC_RPS_SECRET: str
    EC_RPS_ALLOWED_ORIGINS: list[str]
    EC_RPS_DNS_VIEW: str
    EC_RPS_DNS_ZONE: str
    EC_RPS_ACME_CA: str

    EC_BUBBLE: str
    EC_ENV: str

    RETRY_WAIT_MULTIPLIER: int | float
    RETRY_WAIT_MAX: int | float

    DNS_LOOKUP_TIMEOUT: int
    DNS_LOOKUP_RETRY_INTERVAL: int

    RECONCILIATION_INTERVAL: int
