import secrets

from decouple import config

DEBUG: bool = config("DEBUG", default=False, cast=bool)
BIND_PORT: int = config("PORT", default=8000, cast=int)

NETBOX_API: str = config("NETBOX_API")
NETBOX_TOKEN: str = config("NETBOX_TOKEN")

EC_RPS_SECRET: str = secrets.token_hex(32)
EC_RPS_ALLOWED_ORIGINS: list[str] = config(
    "EC_RPS_ALLOWED_ORIGINS",
    default="*",
    cast=lambda val: [origin.strip() for origin in val.split(",")],
)

EC_RPS_DNS_VIEW: str = config("EC_RPS_DNS_VIEW")
EC_RPS_DNS_ZONE: str = config("EC_RPS_DNS_ZONE")
EC_RPS_ACME_CA: str = config("EC_RPS_ACME_CA", default="letsencrypt")

EC_BUBBLE: str = config("EC_BUBBLE")
EC_ENV: str = config("EC_ENV")

RETRY_WAIT_MULTIPLIER = 1
RETRY_WAIT_MAX = 10

DNS_LOOKUP_TIMEOUT = 1 * 60 * 60  # 1 hour
DNS_LOOKUP_RETRY_INTERVAL = 5 * 60  # 5 minutes

RECONCILIATION_INTERVAL = 5 * 60  # 5 minutes
