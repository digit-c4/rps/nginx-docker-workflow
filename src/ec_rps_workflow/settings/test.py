DEBUG: bool = True
BIND_PORT: int = 8000

NETBOX_API: str = "http://localhost"
NETBOX_TOKEN: str = "s3cr3t"

EC_RPS_SECRET: str = "s3cr3t"
EC_RPS_ALLOWED_ORIGINS: list[str] = ["*"]

EC_RPS_DNS_VIEW: str = "INTERNAL"
EC_RPS_DNS_ZONE: str = "ec.local"
EC_RPS_ACME_CA: str = "letsencrypt"

EC_BUBBLE: str = "test"
EC_ENV: str = "test"

RETRY_WAIT_MULTIPLIER = 0.01
RETRY_WAIT_MAX = 0.1

DNS_LOOKUP_TIMEOUT = 0.5
DNS_LOOKUP_RETRY_INTERVAL = 0.1

RECONCILIATION_INTERVAL = 0.1
