## BASE
ARG ALPINE_RELEASE="3.20"
FROM python:3.12-alpine${ALPINE_RELEASE} AS base

RUN set -ex && \
    pip install poetry && \
    poetry config virtualenvs.in-project true

## Python VENV configuration
FROM scratch AS project-context

ADD pyproject.toml poetry.lock /workspace/

## Source Tree
FROM scratch AS sources

ADD src /workspace/src

## Test Suite
FROM scratch AS test-suite

ADD tests /workspace/tests

## Python VENV production dependencies
FROM base AS dependencies

COPY --from=project-context /workspace /workspace
COPY --from=sources /workspace/src /workspace/src
WORKDIR /workspace

ADD README.md /workspace/README.md
RUN poetry install --without dev

## Python VENV development dependencies
FROM base AS dev-dependencies

COPY --from=dependencies /workspace /workspace
WORKDIR /workspace

RUN poetry install --no-root --only dev

## CI steps
FROM base AS ci

COPY --from=project-context /workspace /workspace
COPY --from=sources /workspace/src /workspace/src
COPY --from=test-suite /workspace/tests /workspace/tests
COPY --from=dev-dependencies /workspace/.venv /workspace/.venv
WORKDIR /workspace

ADD docker/docker-entrypoint.sh /docker-entrypoint.sh

RUN poetry run isort --check .
RUN poetry run black --check .
RUN poetry run mypy src
RUN poetry run pytest

## Runner
FROM base AS runner

COPY --from=project-context /workspace /workspace
COPY --from=sources /workspace/src /workspace/src
COPY --from=dependencies /workspace/.venv /workspace/.venv
COPY --from=ci /docker-entrypoint.sh /docker-entrypoint.sh
WORKDIR /workspace

RUN chmod +x /docker-entrypoint.sh

ENV PORT="8000"

ENV NETBOX_API=""
ENV NETBOX_TOKEN=""

ENV EC_RPS_ALLOWED_ORIGINS="*"

ENV EC_RPS_DNS_VIEW=""
ENV EC_RPS_DNS_ZONE=""
ENV EC_RPS_ACME_CA=""

ENV EC_BUBBLE=""
ENV EC_ENV=""

ENTRYPOINT ["/docker-entrypoint.sh"]